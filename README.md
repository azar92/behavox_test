**Language:** Python 3

**Dependencies:**

* nltk – natural language processing toolkit for Python. Install via pip: pip install nltk

* datefinder – module to extract dates in various formats. Install via pip: pip install datefinder

* geotext – module to extract names of cities / countries. Install via pip: pip install geotext

**Source files description:**

**classifier.py** 
Script to train a naïve Bayes classifier against training data in directory ‘train_data_set’

**extractor.py** 
Script contains functions to extract features from text

**test_classifier.py** 
Script to evaluate classifier, saved in file ‘classifier.pickle’ on test set in directory ‘test_data_set’

**inference.py** 
Script to classify single file named ‘test’

**inference_cmd.py** 
Script to classify custom file with custom classifier.
Should be run as: python3 inference_cmd.py classifier_path file_path



**Trained classifier file:** classifier.pickle

**Task documentation:** Documentation.pdf



**To train, evaluate and apply classifier from scratch:**

1. Run classifier.py : python3 classifier.py
This script will train a classifier and save it to ‘classifier.pickle’
You need to have directory train_data_set with three sub-categories: other, sportent, transport

2. Run test_classifier.py: python3 test_classifier.py
This script will evaluate performance of classifier, saved in ‘classifier.pickle’
You need to have directory test_data_set with three sub-categories: other, sportent, transport

3. Run inference_cmd.py with two arguments – classifier file path an test file path, for example:
python3 inference_cmd.py classifier.pickle test
This command will apply classifier, saved in ‘classifier.pickle’ to classify file named test