import extractor
import os
import random
import nltk
import pickle

# Training data directory

TRAINING_DATA_DIR = "train_data_set"

# Feature description is presented in extractor.py and in supporting documentation


def transform_to_features(text):
    is_date_time = extractor.is_date_present(text)
    is_seat = extractor.is_seat_present(text)
    is_row_seat = extractor.is_seat_row_present(text)
    is_kick_off = extractor.is_kick_off_present(text)
    is_transport = extractor.is_transport_present(text)
    is_ticket = extractor.is_ticket_present(text)
    is_departure = extractor.is_departure_present(text)
    is_journey = extractor.is_journey_present(text)
    is_price = extractor.is_price_present(text)
    is_location = extractor.is_location_present(text)
    is_airplane = extractor.is_airplane_set_present(text)
    is_passenger = extractor.is_passenger_details(text)
    is_cultural = extractor.is_cultural_present(text)
    is_valid = extractor.is_valid_or_issued(text)
    is_invoice = extractor.is_invoice_receipt(text)
    is_geographical = extractor.is_geographical(text)
    length = extractor.document_length(text)

    return {'is_date_time': is_date_time,
            'is_seat': is_seat,
            'is_row_seat': is_row_seat,
            'is_kick_off': is_kick_off,
            'is_transport': is_transport,
            'is_ticket': is_ticket,
            'is_departure': is_departure,
            'is_journey': is_journey,
            'is_price': is_price,
            'is_location': is_location,
            'is_airplane': is_airplane,
            'is_passenger': is_passenger,
            'is_cultural': is_cultural,
            'is_valid': is_valid,
            'is_invoice': is_invoice,
            'is_geographical': is_geographical,
            'length': length}


# Loading data


def load_data(data_dir, encoding="utf-8"):
    examples = []

    for name in sorted(os.listdir(data_dir)):
        path = os.path.join(data_dir, name)
        if os.path.isdir(path):
            label_name = name
            for f_name in sorted(os.listdir(path)):
                f_path = os.path.join(path, f_name)
                f = open(f_path, encoding=encoding)
                text = f.read()
                if len(text) > 0:
                    examples.append((text, label_name))
                f.close()

    return examples


def create_classifier(data_set, classifier_type="NB"):

    # Create and train either naive bayes or decision tree

    if classifier_type == "NB":
        created_classifier = nltk.NaiveBayesClassifier.train(data_set)
    elif classifier_type == "DT":
        created_classifier = nltk.DecisionTreeClassifier.train(data_set)

    return created_classifier


def save_classifier(classifier_to_save, filename="classifier.pickle"):
    f = open(filename, 'wb')
    pickle.dump(classifier_to_save, f)
    f.close()


if __name__ == "__main__":

    samples = load_data(TRAINING_DATA_DIR)

    random.shuffle(samples)

    train_set = [(transform_to_features(text), category) for (text, category) in samples]

    classifier = create_classifier(train_set, classifier_type="NB")

    file_to_save = "classifier.pickle"

    save_classifier(classifier, file_to_save)

    print("Classifier was successfully trained and saved to", file_to_save, "\n",
          "You can now evaluate it on testing set by running test_classifier.py")









