import pickle
from classifier import transform_to_features

# Loading classifier

f = open('classifier.pickle', 'rb')
classifier = pickle.load(f)
f.close()

# Loading text file to test

test_document_path = "test"
f = open(test_document_path)
text = f.read()
f.close()

res = classifier.prob_classify(transform_to_features(text))

labels = ["sportent", "transport", "other"]

print("Probabilities: ", "\n")

for label in labels:
    print(label, round(res.prob(label), 3))


print("\nPredicted class:", res.max())
