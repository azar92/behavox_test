# Script to extract features from text

import datefinder
import re
import string
import nltk
import geotext

# Helper function to remove punctuation and split text onto list tokens


def process_text(text):
    text = text.lower()
    translator = str.maketrans({key: " " for key in string.punctuation})
    text_without_punctuation = text.translate(translator)
    tokens = nltk.word_tokenize(text_without_punctuation)
    return tokens


# Helper functions to estimate document length


def count_number_of_words(text):
    tokens = process_text(text)
    return len(tokens)


def document_length(text, short_boundary=100, long_boundary=500):
    length = count_number_of_words(text)

    if length < short_boundary:
        return 'short'
    elif short_boundary <= length <= long_boundary:
        return 'medium'
    else:
        return 'long'


# Define a helper function that firstly searches for n-grams (strong indicators) - if any, returns 'strong',
# then processes text and searches for key words - if any, computes a ratio: number of indicative words divided by
# total number of words. If ratio is greater then certain boundary, return 'medium', else return 'weak'

def is_ngram_or_word_present(text, ngrams, words, medium_boundary=0.02):

    for ngram in ngrams:
        if ngram in text.lower():
            return 'strong'

    tokens = process_text(text)

    matches = 0

    for token in tokens:
        if token in words:
            matches += 1

    if matches == 0:
        return 'no'

    if (matches / len(tokens)) > medium_boundary:
        return 'medium'
    else:
        return 'weak'


# If date in any common format is explicitly mentioned in document, it can be an indicator of some kind of ticket

def is_date_present(text):

    # First try to extract dates in conventional format with datefinder lib

    dates = list(datefinder.find_dates(text, strict=True))

    if len(dates) > 0:
        return 'yes'

    tokens = process_text(text)

    # Search for date of format like 26 Jan 2016 (26 January 2016)

    months = ["january", "jan", "february", "feb", "march", "mar", "april", "apr", "may", "june", "jun", "july",
              "jul", "august", "aug", "september", "sep", "october", "oct", "november", "nov", "december", "dec"]

    if len(tokens) >= 2:

        for i in range(0, len(tokens) - 1):
            if tokens[i] in months:
                if i == 0:
                    if tokens[i+1].isdigit():
                        return 'yes'
                else:
                    if tokens[i+1].isdigit() or tokens[i-1].isdigit():
                        return 'yes'

    return 'no'


# If time in any common format is explicitly mentioned in document, it can be an indicator of some kind of ticket

def is_time_present(text):

    text_l = text.lower()
    text_trim_ws = text_l.replace(" ", "")
    res_time = re.search(r'\d{1,2}((am|pm)|(:\d{1,2})(am|pm)?)', text_trim_ws)

    if res_time is not None:
        return 'yes'

    return 'no'


# Strong indicator - if both date and time are present

def is_date_time_present(text):
    is_time = is_time_present(text)
    is_date = is_date_present(text)

    if is_time == 'yes' and is_date == 'yes':
        return 'strong'
    elif is_time == 'yes' or is_date == 'yes':
        return 'weak'
    else:
        return 'no'


# Kick off fraze is a strong indicator of some kind of sports event

def is_kick_off_present(text):
    variants = ["kick off", "kickoff", "kick-off"]

    for variant in variants:
        if variant in text.lower():
            return 'yes'

    return 'no'


# Seat (or seats) word, but without 'row' word present is an indicator of some kind of transport ticket

def is_seat_present(text):

    is_seat = False
    is_row = False

    tokens = process_text(text)

    for token in tokens:
        if token in ['seat', 'seats']:
            is_seat = True
        elif token == 'row':
            is_row = True

    if "seat no" in text.lower() and not is_row:
        return 'strong'

    if is_seat and not is_row:
        return 'weak'
    else:
        return 'no'


# Row-seat pair in a short text patch is a very strong indicator of some entertainment / sport ticket

def is_seat_row_present(text):
    is_seat = False
    is_row = False

    tokens = process_text(text)

    for token in tokens:
        if token == 'seat':
            is_seat = True
        elif token == 'row':
            is_row = True

    if is_seat and is_row:
        # If any digits in document (can indicate row/seat numbers)

        are_digits = any(char.isdigit() for char in text.lower())

        # Entertainment tickets tend to have short or medium length

        length = document_length(text)

        if length in ["short", "medium"] and are_digits:
            return 'strong'
        else:
            return "weak"

    return 'no'


# Words like ticket, eTicket, etc. are strong indicators of this kind of document

def is_ticket_present(text):
    ticket_words = ["ticket", "e-ticket", "eticket", "travelcard", "tkt"]
    ticket_ngrams = ["bus ticket", "boarding pass", "travel card", "ticket no", "ticket number", "ticket type",
                     "electronic ticket"]

    result = is_ngram_or_word_present(text, ticket_ngrams, ticket_words)

    return result


# Words indicating transport, like train or bus, can be indicators of transport document

def is_transport_present(text):
    transport_words = ["train", "bus", "tram", "subway", "metro", "airplane"]

    tokens = process_text(text)

    for token in tokens:
        if token in transport_words:
            return 'yes'

    return 'no'


# Word 'departure' and its forms is a strong indicator of transport document

def is_departure_present(text):
    departure_words = ["departure", "departs", "dep", "departing"]
    departure_ngrams = ["departure time", "time of departure"]

    result = is_ngram_or_word_present(text, departure_ngrams, departure_words)

    return result

# Words indicating some kind of journey or trip might be an indicator of transport document


def is_journey_present(text):
    journey_words = ["journey", "trip", "flight", "travel", "route", "destination"]
    journey_ngrams = ["trip information", "journey details", "travel information", "trip details", "date of journey"]

    result = is_ngram_or_word_present(text, journey_ngrams, journey_words)

    return result


# Words and symbols indicating price can point to some kind of ticket.


def is_price_present(text):
    price_words = ["fare", "$", "£", "USD", "price"]
    price_strong = ["total fare", "admission"]

    result = is_ngram_or_word_present(text, price_strong, price_words)

    return result


# If words about some event location are present in text, it can be an indicator of sport / entertainment event


def is_location_present(text):
    location_words = ["arena", "stadium", "opera", "hall", "theatre",
                      "amphitheatre", "amphitheater", "theater", "museum"]

    for word in location_words:
        if word in text.lower():
            return 'yes'

    return 'no'


# If four words - gate, boarding, seat, flight are present in document - it is a strong indicator of
# airplane ticket / boarding pass

def is_airplane_set_present(text):

    airplane_words = ["gate", "boarding", "seat", "flight"]

    if all(word in text.lower() for word in airplane_words):
        return 'yes'
    else:
        return 'no'


# Phrase 'passenger information' or 'passenger details' is a very strong indicator of transport ticket

def is_passenger_details(text):
    passenger_phrases = ["passenger details", "passenger information", "passenger name"]

    for phrase in passenger_phrases:
        if phrase in text.lower():
            return 'yes'

    return 'no'


# If some kind of cultural event is present, it can be an indicator of entertainment ticket

def is_cultural_present(text):
    cultural_events = ["exhibition", "festival", "expo", "wedding"]

    for phrase in cultural_events:
        if phrase in text.lower():
            return 'yes'

    return 'no'


# Phrases like 'issued by' or 'valid until' tend to appear on various kinds of transport tickets

def is_valid_or_issued(text):
    ticket_phrases = ["issued by", "valid until", "issue date", "valid from"]

    for phrase in ticket_phrases:
        if phrase in text.lower():
            return 'yes'

    return 'no'

# There are many indicators of transport/entertainment tickets like datetime objects, prices, etc.
# which can appear in some documents like invoices and receipts. However, such words do not appear on tickets
# Thus their presence in document can be a strong indicator, that such a document is not relevant


def is_invoice_receipt(text):
    invoice_receipt = ["invoice", "receipt"]

    for word in invoice_receipt:
        if word in text.lower():
            return 'yes'
    return 'no'


# Geographical names (cities and countries) tend to appear in any kinds of transport tickets,
# however, standalone they are week indicators. In combination with 'from-to' pair, they
# become a strong indicator of transportation document

def is_geographical(text):
    places = geotext.GeoText(text.upper())

    if len(places.cities) > 0 or len(places.countries) > 0:
        if all(word in text.lower() for word in ["from", "to"]):
            return 'strong'
        else:
            return 'week'
    return 'no'

