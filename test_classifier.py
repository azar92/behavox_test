import pickle
from classifier import transform_to_features, load_data
import nltk

# Loading classifier

f = open('classifier.pickle', 'rb')
classifier = pickle.load(f)
f.close()

# Directory with test data set

TEST_DATA_DIR = "test_data_set"

# Loading test set and transforming it into features

samples = load_data(TEST_DATA_DIR)
test_set = [(transform_to_features(text), category) for (text, category) in samples]


# Evaluating and displaying accuracy

accuracy = nltk.classify.accuracy(classifier, test_set)
print("Accuracy :", round(accuracy, 2) * 100, "%", "\n")

# Error analysis

print("Errors: ")

errors = []

for (text, category) in samples:
    guess = classifier.classify(transform_to_features(text))
    if guess != category:
        errors.append((category, guess, text))

for (category, guess, text) in errors:
    print("Correct category: ", category)
    print("Guess: ", guess)
    print("Text:\n", text)
