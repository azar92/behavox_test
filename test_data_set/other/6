Remember when I nearly died learning to drive a standard shift car while backpacking around the Alps? That trip made me a little wiser, but maybe not wise enough. Rather than thanking my lucky stars that I survived and hanging up the keys, I got behind the wheel the very next week.

The next stop on this same cold January adventure with my friend Lisa was Barcelona. It was warmer there and after nearly freezing in Austria, the thought of thawing in the sun and dipping our toes in the Mediterranean became an obsession. And because we were still too young to learn from our driving misadventures days before, we got off the train and walked straight into a car hire shop to rent some wheels to drive the coast of Spain.

They handed us the keys to a blue Seat and we hit the road. Of course it was standard shift, but we were experts now we believed, and even made it out of the rental lot without stalling.

The seaside was as perfect and sunny as we imagined, and since we were both Florida girls, the coastal road was much less stressful to attack than the Alps. Our biggest challenge on this driving adventure actually came when we arrived back to Barcelona.

Oddly, our hostel didn’t have valet service and we had to figure out what to do with our car overnight. We drove around the city center in circles searching for free parking without luck. After nearly dying when we drove down a one way boulevard the wrong way, we decided to give up our frugality and pulled into an underground parking garage. We paid our pesetas, took our ticket and as we exited we noted down the landmarks so we’d be able to retrieve our wheels. These landmarks were the entrance to a large metro station and of course, the giant blue P sign to mark the parking garage. Certainly we’d have no problem finding our car in the morning.

Wrong. The metro station we had parked near was in fact large. So large, that the next morning we realized it had many  entrances, all of which happened to have multiple underground parking garages nearby. It didn’t take long for us to conclude that we had lost our car.

We had only one day in our travel plan to see Barcelona and we quickly realized we were going to spend the best part of it searching underground parking lots with broken Spanish and charades.

In the end, we missed the museum’s, skipped the sangria and only had time to fly by the Sagrada Familia once we had finally retrieved and returned our rental. However, we did get a great tour of Barcelona underground.

You’ve heard it said that the journey is the destination. I certainly believe that traveling isn’t only about the sights we see, and this experience was one of those times I learned to believe this truth. Exploring isn’t just about the museum, but getting to know the parking lot that lies underneath.

Of course, the other lesson I should have learned on this trip was to always remember where I park. But I won’t lie, it is one I’m still mastering.

Enjoy the journey even if you never make it out of the parking garage.
