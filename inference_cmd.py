import pickle
import sys
from classifier import transform_to_features
import os.path


classifier_name = sys.argv[1]
file_name = sys.argv[2]

if not os.path.isfile(classifier_name) or not os.path.isfile(file_name):
    print("Classifier or test file can not be found")
else:
    # Loading classifier
    f = open(classifier_name, 'rb')
    classifier = pickle.load(f)
    f.close()

    # Loading text file to test

    test_document_path = file_name
    f = open(test_document_path)
    text = f.read()
    f.close()

    if len(text) > 0:

        res = classifier.prob_classify(transform_to_features(text))

        labels = ["sportent", "transport", "other"]

        print("Probabilities: ", "\n")

        for label in labels:
            print(label, round(res.prob(label), 3))

        print("\nPredicted class:", res.max())
    else:
        print("Text has zero length")
